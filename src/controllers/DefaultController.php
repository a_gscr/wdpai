<?php
require_once 'AppController.php';
class DefaultController extends AppController {
    public function index()
    {
        $this->render('login');
    }
    public function dashboard()
    {
        $this->render('dashboard');
    }
    public function register()
    {
        $this->render('register');
    }
    public function statistics()
    {
        $this->render('statistics');
    }
    public function settings()
    {
        $this->render('settings');
    }
    public function logout()
    {
        $this->render('loggedout');
    }
}