<?php

require_once 'AppController.php';
require_once __DIR__.'/../models/Offer.php';
require_once __DIR__.'/../repository/OfferRepository.php';

class OffersController extends AppController
{
    const MAX_FILE_SIZE = 1024*1024;
    const SUPPORTED_TYPES=['image/png','image/jpg'];
    const UPLOAD_DIRECTORY='/../public/uploads/';

    private $messages = [];
    private $offerRepository;

    public function __construct(){
        parent::__construct();
        $this->offerRepository = new OfferRepository();
    }
    public function offers()
    {   $offers=$this->offerRepository->getOffers();
        return $this->render("offers",[
            'offers'=>$this->offerRepository->getOffers()]);
    }

    public function addAd()
    {
        if($this->isPost() && is_uploaded_file($_FILES["file"]['tmp_name']) && $this->validate($_FILES['file'])){

            move_uploaded_file(
                $_FILES["file"]['tmp_name'],
                dirname(__DIR__).self::UPLOAD_DIRECTORY.$_FILES['file']['name']
            );

            $offer = new Offer($_POST['url'],$_POST['make'],$_POST['model'],$_POST['price'],$_POST['km'],$_POST['year'],
                $_POST['firstreg'],$_POST['fuel'],$_FILES['file']['name']);
            $this->offerRepository->addOffer($offer);

            return $this->render("offers",[
                'offers'=>$this->offerRepository->getOffers(),
                'messages'=>$this->messages,'offer'=>$offer]);
        }
        return $this->render("offers",['messages'=>$this->messages]);
    }

    public function statistics(){

    }

    private function validate(array $file):bool{
        if($file['size'] > self::MAX_FILE_SIZE){
            $this->messages[]="Plik jest za duży.";
            return false;

        }
        if(!isset($file['type']) && !in_array($file['type'],self::SUPPORTED_TYPES)){
            $this->messages[]="Typ pliku nie jest wspierany.";
            return false;
        }
        return true;
    }

}