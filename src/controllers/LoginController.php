<?php

require_once 'AppController.php';
require_once __DIR__.'/../models/User.php';
require_once __DIR__.'/../repository/UserRepository.php';

class LoginController extends AppController
{
    public function login()
    {
        $userRepository = new UserRepository();

        if ($this->isGet()) {
            return $this->render('login');
        }

        $email = $_POST["email"];
        $password = $_POST["password"];

        $user = $userRepository->getUser($email);

        if (!$user) {
            return $this->render('login', ['messages' => ['Użytkownik nie istnieje']]);
        }

        if ($user->getEmail() !== $email) {
            return $this->render('login', ['messages' => ['Użytkownik nie istnieje']]);
        }
        if ($user->getPassword() !== $password) {
            return $this->render('login', ['messages' => ['Podano niepoprawne hasło']]);
        }
        $url = "http://$_SERVER[HTTP_HOST]";
        //  return $this->render('offers');
        header("Location: {$url}/dashboard");
    }

    public function addUser()
    {
        $userRepository = new UserRepository();

        if ($this->isGet()) {
            return $this->render('register');
        }

        $email = $_POST["email"];
        $password = $_POST["password"];
        $nick = $_POST["nick"];


        $user = new User($nick, $email, $password);
        $this->userRepository->addUser($user);

        return $this->render("offers");
    }



}