<?php


require_once 'Repository.php';
require_once __DIR__.'/../models/Offer.php';

class OfferRepository extends Repository
{

    public function getOffer(int $id): ?Offer
    {
        $stmt = $this->database->connect()->prepare(
            '
            SELECT * FROM offer WHERE id = :id
            '
        );
        $stmt->bindParam(":email", $id, PDO::PARAM_INT);
        $stmt->execute();
        $offer = $stmt->fetch(PDO::FETCH_ASSOC);

        if ($offer == false) {
            return null;
        }
        return new Offer(
            $offer['url'],
            $offer['make'],
            $offer['model'],
            $offer['price'],
            $offer['km'],
            $offer['year'],
            $offer['firstreg'],
            $offer['fuel'],
            $offer['image']
        );
    }

    public function addOffer(Offer $offer): void
    {
        $stmt = $this->database->connect()->prepare('
            INSERT INTO offers (user_id,make,model,price,url,year,firstreg,fuel,km,image)
            VALUES (?,?,?,?,?,?,?,?,?,?)
        ');

        $assignedById = 2;

        $stmt->execute([
            $assignedById,
            $offer->getMake(),
            $offer->getModel(),
            $offer->getPrice(),
            $offer->getUrl(),
            $offer->getYear(),
            $offer->getFirstreg(),
            $offer->getFuel(),
            $offer->getKm(),
            $offer->getImage()
        ]);
    }

    public function getOffers(): array
    {
        $result = [];

        $stmt = $this->database->connect()->prepare('
            SELECT * FROM offers;
      ');
        $stmt->execute();

        $offers = $stmt->fetchAll(PDO::FETCH_ASSOC);

        foreach ($offers as $offer) {
            $result[] = new Offer(
                $offer['url'],
                $offer['make'],
                $offer['model'],
                $offer['price'],
                $offer['km'],
                $offer['year'],
                $offer['firstreg'],
                $offer['fuel'],
                $offer['image']
            );
        }

        return $result;
    }

}