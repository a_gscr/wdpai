<?php

class Offer
{
    private $url;
    private $make;
    private $model;
    private $price;
    private $km;
    private $year;
    private $firstreg;
    private $fuel;
    private $image;


    public function getUrl()
    {
        return $this->url;
    }


    public function setUrl(string $url): void
    {
        $this->url = $url;
    }

    public function getMake()
    {
        return $this->make;
    }

    public function setMake(string $make): void
    {
        $this->make = $make;
    }

    public function getModel()
    {
        return $this->model;
    }


    public function setModel(string $model): void
    {
        $this->model = $model;
    }


    public function getPrice()
    {
        return $this->price;
    }

    public function setPrice(int $price): void
    {
        $this->price = $price;
    }


    public function getKm()
    {
        return $this->km;
    }


    public function setKm(int $km): void
    {
        $this->km = $km;
    }


    public function getYear()
    {
        return $this->year;
    }


    public function setYear(string $year): void
    {
        $this->year = $year;
    }


    public function getFirstreg()
    {
        return $this->firstreg;
    }

    public function setFirstreg(string $firstreg): void
    {
        $this->firstreg = $firstreg;
    }


    public function getFuel()
    {
        return $this->fuel;
    }

    public function setFuel(string $fuel): void
    {
        $this->fuel = $fuel;
    }

    public function getImage()
    {
        return $this->image;
    }


    public function setImage(string $image): void
    {
        $this->image = $image;
    }



    public function __construct($url, $make, $model, $price, $km, $year, $firstreg, $fuel, $image)
    {
        $this->url = $url;
        $this->make = $make;
        $this->model = $model;
        $this->price = $price;
        $this->km = $km;
        $this->year = $year;
        $this->firstreg = $firstreg;
        $this->fuel = $fuel;
        $this->image = $image;
    }

}