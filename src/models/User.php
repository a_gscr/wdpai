<?php

class User
{
    private $email;
    private $nick;
    private $password;



    public function __construct(string $email, string $nick, string $password)
    {
        $this->email = $email;
        $this->password = $password;
        $this->$nick = $nick;
    }


    public function getEmail(): string
    {
        return $this->email;
    }


    public function setEmail(string $email)
    {
        $this->email = $email;
    }


    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password)
    {
        $this->password = $password;
    }

    public function getNick(): string
    {
        return $this->nick;
    }

    public function setNick(string $nick)
    {
        $this->name = $nick;
    }



}