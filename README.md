# Autly
#### Autly to aplikacja, dzięki której wybrane przez Ciebie ogłoszenia z różnych portali ogłoszeniowych zostają umieszczone w jednym miejscu!

[![N|Solid](https://i.imgur.com/n5Q1G17.png)](autly.pl)
[![N|Solid](https://i.imgur.com/RNP8vwm.png)](autly.pl)
[![N|Solid](https://i.imgur.com/3Al9Sfg.png)](autly.pl)

## Docker - uruchomienie aplikacji

Autly jest bardzo proste w instalacji. Wystarczy pobrać aplikację Docker Desktop, otworzyć główny folder w terminalu i wpisać:

```sh
docker-compose build
```
i następnie
```sh
docker-compose run
```

Od teraz możesz testować aplikację, wszelkie uwagi mile widziane!

## License
                     GNU GENERAL PUBLIC LICENSE
                       Version 3, 29 June 2007

 Copyright (C) 2007 Free Software Foundation, Inc. <https://fsf.org/>
 Everyone is permitted to copy and distribute verbatim copies
 of this license document, but changing it is not allowed.