<?php

require 'Routing.php';

$path = trim($_SERVER['REQUEST_URI'], '/');
$path = parse_url( $path, PHP_URL_PATH);

Router::get('', 'DefaultController');
Router::get('dashboard', 'DefaultController');
Router::get('register', 'DefaultController');
Router::get('offers', 'OffersController');
Router::post('login', 'LoginController');
Router::post('addAd', 'OffersController');
Router::post('statistics', 'OffersController');
Router::post('addUser', 'LoginController');

Router::run($path);