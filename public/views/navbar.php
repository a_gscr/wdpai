<!DOCTYPE html>
<head>
    <meta charset="UTF-8">
    <script src="https://kit.fontawesome.com/0aec6c82a0.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" type="text/css" href="public/css/navbar.css">
    <title>Autly Dashboard</title>
</head>
<body>
    <header class="header">
        <img
          src="public/img/logo.svg"
          class="logo"
        />

        <nav class="active navbar">
          <ul class="navbar-list">
            <li><a class="navbar-link" href="/offers"><i class="fa-solid fa-plus"></i>Oferty</a></li>
            <li><a class="navbar-link" href="/statistics"><i class="fa-solid fa-chart-line"></i>Statystyki</a></li>
            <li><a class="navbar-link" href="/settings"><i class="fa-solid fa-gears"></i>Ustawienia</a></li>
            <li><a class="navbar-link logout" href="/"><i class="fa-solid fa-arrow-right-from-bracket"></i></a></li>
          </ul>
        </nav>

      </header>



</body>