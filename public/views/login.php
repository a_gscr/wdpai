<!DOCTYPE html>
<head>
    <link rel="stylesheet" type="text/css" href="public/css/login.css">
    <link rel="stylesheet" type="text/css" href="public/css/style.css">
    <title>Zaloguj się</title>
</head>
<body>
<div class="form-main-container">
    <div class="form-container">
        <div class="logo">
            <img class="logo-image" src="public/img/logo.svg">
        </div>
        <div class="messages">
            <?php if(isset($messages)){
                foreach ($messages as $message)
                {
                    echo $message;
                }
            }
            ?>
        </div>
        <div class="login-form">
            <form  class="login" action="login" method="POST">

                <input name="email" type="text" placeholder="Podaj adres e-mail">
                <input name="password" type="password" placeholder="Podaj hasło">
                <input type="submit" value="Zaloguj"><br>
                <span><a href="/register">Kliknij, aby zarejestrować nowe konto</a></span>
            </form>
        </div>
    </div>
</div>
</body>