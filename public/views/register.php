<!DOCTYPE html>
<head>
    <link rel="stylesheet" type="text/css" href="public/css/register.css">
    <link rel="stylesheet" type="text/css" href="public/css/style.css">
    <script src="public/scripts/script.js" defer></script>
    <title>Zaloguj się</title>
</head>
<body>
<div class="form-main-container">
    <div class="form-container">
        <div class="logo">
            <img class="logo-image" src="public/img/logo.svg">
        </div>

        <div class="login-form">
            <form  class="login" action="addUser" method="POST">
                <div class="messages">
                    <?php if(isset($messages)){
                    foreach ($messages as $message)
                        {
                    echo $message;
                            }
                }
                ?>
                </div>
                <input name="email" type="text" placeholder="Podaj adres e-mail">
                <input name="nick" type="text" placeholder="Podaj nick">
                <input name="imie" type="text" placeholder="Podaj imie">
                <input name="nazwisko" type="text" placeholder="Podaj nazwisko">
                <input name="phone" type="text" placeholder="Podaj nr telefonu">
                <input name="password" type="password" placeholder="Podaj hasło">
                <input name="confirmedPassword" type="password" placeholder="Podaj hasło">
                <input type="submit" value="Zarejestruj">
                <br>

            </form>
        </div>
    </div>
</div>
</body>