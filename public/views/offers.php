<!DOCTYPE html>
<head>
    <link rel="stylesheet" type="text/css" href="public/css/style.css">
    <link rel="stylesheet" type="text/css" href="public/css/offers.css">
    <script src="https://kit.fontawesome.com/0aec6c82a0.js" crossorigin="anonymous"></script>
    <script
    <title>Oferty</title>
</head>
<body>
<?php include 'navbar.php'?>
<div class="dashboard">
<div class="left-nav-panel">
        <div class="offer-form">
            <h2>Podaj parametry ogłoszenia</h2>
            <form  class="login" action="addAd" method="POST" enctype="multipart/form-data">
                <input name="url" type="text" placeholder="URL">
                <br>
                <input name="make" type="text" placeholder="Marka">
                <br>
                <input name="model" type="text" placeholder="Model">
                <br>
                <input name="price" type="text" placeholder="Cena">
                <br>
                <input name="km" type="text" placeholder="Przebieg">
                <br>
                <input name="year" type="text" placeholder="Rok">
                <br>
                <input name="firstreg" type="text" placeholder="Pierwsza rejestracja">
                <br>
                <input name="fuel" type="text" placeholder="Typ paliwa">
                <br>
                Wybierz obraz (opcjonalnie)<br>
                <input type="file" name="file">
                <br>
                <input type="submit" value="Zapisz">
            </form>
        </div>
</div>
    <div class="ad-list">
        <table>
            <tr>
                <th>Zdjęcie</th>
                <th>Marka</th>
                <th>Model</th>
                <th>Cena</th>
                <th>Url</th>
                <th>Rok</th>
                <th>Pierwsza rejestracja</th>
                <th>Typ paliwa</th>
                <th>Przebieg</th>
            </tr>
        <?php foreach($offers as $offer): ?>
            <tr>
            <td><img class="list-image" src="public/uploads/<?= $offer->getImage();?>"></td>
                <td><?= $offer->getMake(); ?></td>
                <td><?= $offer->getModel(); ?></td>
                <td><?= $offer->getPrice(); ?></td>
                <td><a href="<?= $offer->getUrl(); ?>">Kliknij tutaj</td>
                <td><?= $offer->getYear(); ?></td>
                <td><?= $offer->getFirstreg(); ?></td>
                <td><?= $offer->getFuel(); ?></td>
                <td><?= $offer->getKm(); ?></td>
            </tr>
    <?php endforeach; ?>
    </table>
    </div>
</div>

</body>